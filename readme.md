# Introduction to Jupyter

This repository contains some tricks for using jupyter notebooks, such as:

- Loading a CSV file
- Displaying tabular data using pandas
- Formatting tabular data (highlight cells)



## Contact

Auraham Camacho `auraham.cg@gmail.com`